from Shared import EventHook
from Shared import plugin_manager
from BotCore.BotShared import BotShared
from BotOutputs import BotOutputs
from BotInputs import BotInputs

import uuid
import random
import importlib


class MyDict(dict):
    # TODO: move this into shared and work out how to ingec teh event hook name
    def __init__(self, *arg, **kw):
        super(MyDict, self).__init__(*arg, **kw)
        self.on_value_update = EventHook()

    def __setitem__(self, item, value):
        if self.__contains__('Name'):
            self.on_value_update.fire(self['Name'], value)
        else:
            self.on_value_update.fire(self, value)
        super(MyDict, self).__setitem__(item, value)


class Bot:
    """
    Main Bot Class
    """
    def __init__(self):
        self.plugin_manager = plugin_manager.PluginManager(plugin_folder='./plugins')
        self.bot_updated = EventHook();
        self.shared = BotShared()
        self.outputs = BotOutputs()
        self.inputs = BotInputs()
        self.details = None
        self.load_bot_config()
        self.details.on_value_update += self.on_value_update
        for cmd in self.plugin_manager.get_available_plugins():
            self.plugin_manager.load_plugin(cmd)
            self.func_builder(cmd)
            setattr(self.__class__, 'help_' + cmd, self.func_builder(cmd))

    def func_builder(self, name):
        def f(self):
            print(self.plugin_manager.help_hook(name))
        return f

    def load_bot_config(self):
        self.shared.get_config()
        self.details = self.map_to_my_dict(self.shared.details)

        if self.details.__contains__('Name') is False:
            self.details.__setitem__('Name', 'spider-bot-' + str(random.randrange(50)))

        if self.details.__contains__('') is False:
            self.details.__setitem__('guid', str(uuid.uuid4()))

        if self.details.__contains__('Outputs') is True:
            self.configuare_outputs(self.details['Outputs'])

        if self.details.__contains__('Inputs') is True:
            self.configuare_inputs(self.details['Inputs'])

    def send_update(self):
        self.bot_updated.fire()

    def new_command(self, command, value):
        if command == 'get_help':
            plugins = self.plugin_manager.get_available_plugins()
            _help = []
            for plugin in plugins:
                self.plugin_manager.load_plugin(plugin)
                _help.append({'name': plugin, 'help_text': self.plugin_manager.help_hook(plugin)})
                self.plugin_manager.unload_plugin(plugin)
            return _help
        if command in self.plugin_manager.get_available_plugins():
            self.plugin_manager.load_plugin(command)
            result = self.plugin_manager.execute_action_hook('task', { 'bot': self, 'value': value})
            self.plugin_manager.unload_plugin(command)
            return result
        return ''

    def map_to_my_dict(self, d):
        if isinstance(d, dict):
            new_dict = MyDict()
            for k, v2 in d.items():
                if isinstance(v2, dict):
                    new_dict.__setitem__(k, self.map_to_my_dict(v2))
                else:
                        new_dict.__setitem__(k, v2)
            return new_dict
        return

    def configuare_outputs(self, outputs):
        for group in outputs['Groups']:
            _type = outputs['Groups'][group]['Type']
            class_ = getattr(importlib.import_module("BotCore.Outputs"), _type)
            instance = class_(outputs['Groups'][group]['Connections'], self.on_value_update)

            self.outputs.__setattr__(_type, instance)

    def on_value_update(self, name, value):
        self.bot_updated.fire(name, value)

    def configuare_inputs(self, inputs):
        for group in inputs['Groups']:
            _type = inputs['Groups'][group]['Type']
            class_ = getattr(importlib.import_module("BotCore.Inputs." + _type), _type)
            instance = class_()
            self.inputs.__setattr__(_type, instance)


if __name__ == "__main__":
    Bot()
