from Shared import Config
from Bot import Bot
from socketIO_client import SocketIO, BaseNamespace
import json

class ClientNamespace(BaseNamespace):
    BaseNamespace.bot = Bot()

    def on_connect(self):
        data = self.remove_invalid(self.bot.details, ['servo', 'motor'])
        self.emit("NEW BOT", json.dumps(data))
        self.bot.bot_updated += self.on_bot_update

    def remove_invalid(self, d, to_remove):
        if isinstance(d, dict):
            new_dict = dict()
            for k, v_2 in d.items():
                if isinstance(v_2, dict):
                    new_dict.__setitem__(k, self.remove_invalid(v_2, to_remove))
                else:
                    if k not in to_remove:
                        new_dict.__setitem__(k, v_2)
            return new_dict

    def on_disconnect(self):
        return "disconnected"

    def on_reconnect(self):
        print("reconnected")
        data = self.remove_invalid(self.bot.details, ['servo', 'motor'])
        self.emit("NEW BOT", json.dumps(data))

    def on_command(self, data):
        print('on_command' + str(data))

    def on_message(self, data):
        instruction = data.decode('utf-8').split(',', 1)
        command = instruction[0].replace('2', '').replace('[', '').replace('"', '')
        info = None
        print(command)
        if command is not '0':
            if len(instruction) > 1:
                current_data = instruction[1].replace('\\', '').replace(']', '').replace('"', '', 1)[:-1]
                if self.is_json(current_data):
                    info = json.loads(current_data)
            self.parse_command(command, info)

    def parse_command(self, command, data):
        # TODO update this so I know which command the results are for
        results = self.bot.new_command(command, data)
        self.emit("command_results", {'command': command, 'results': results})


    def on_bot_update(self, name, value):
        return_data = {}
        return_data.__setitem__('Name', self.bot.details['Name'])
        return_data.__setitem__(name, value)
        self.emit("UPDATE BOT", json.dumps(return_data))

    def is_json(self, myjson):
        try:
            json.loads(myjson)
        except ValueError as e:
            print(e)
            return False
        return True


if __name__ == "__main__":
    config = Config()

    socketIO = SocketIO(host=config.websokket_url, port=config.websocket_port, Namespace=ClientNamespace)
    clientSocket = socketIO.define(ClientNamespace, '/')
    socketIO.wait()
