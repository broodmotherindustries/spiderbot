from plugins.plugincore import get_manafest
from plugins.plugincore import can_plugin_be_used
from pathlib import Path


def action_task(hook_params):
    manafest = get_details()
    outputs = manafest['Outputs'].split('|')
    inputs = manafest['Inputs'].split('|')
    bot = hook_params['bot']
    can_user = can_plugin_be_used(bot, outputs, inputs)
    if can_user is not True:
        return can_user

    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_left_hip', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_left_leg', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_left_foot', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_right_hip', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_right_leg', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('front_right_foot', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_left_hip', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_left_leg', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_left_foot', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_right_hip', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_right_leg', deg)
    for deg in range(179):
        bot.outputs.SunfounderPCA9685.set_servo('back_right_foot', deg)

def get_details():
    return get_manafest(Path(__file__))

def help_updateBot():
    return 'runs Test 1'






