"""
This is used to manage the plugins and work out if they can be used.

I don't know if I should move all the plugins to the BotCore library so they are shared across bots.

"""
from pathlib import Path
import os
import ruamel.yaml as yaml


def get_manafest(path):
    """
    Loads the manafest file for the plugin
    :param path: the current path of the plugin
    :return: content of the manafest file
    """
    fn = os.path.join(os.path.dirname(path), 'manafest.yaml')
    if Path(fn).is_file():
        with open(fn) as stream:
            try:
                content = yaml.safe_load(stream)
                return  content
            except yaml.YAMLError as exc:
                print(exc)

def can_plugin_be_used(bot, outputs=None, inputs=None):
    """
    Based on the bot and the outputs and inputs in the manafest can this plugin be used.
    :param bot: the current bot
    :param outputs: all outputs the plugin requires
    :param inputs: all inputs the plugin requires
    :return:  True or a string
    """
    if outputs is not None:
        for bot_output in outputs:
            if bot_output:
                try:
                    if getattr(bot.outputs, bot_output) is not AttributeError:
                        pass
                except AttributeError:
                    print('This plugin can not be used by this bot')
                    return 'This plugin can not be used by this bot'

    if inputs is not None :
        for bot_input in inputs:
            if bot_input:
                try:
                    if getattr(bot.inputs, bot_input) is not AttributeError:
                        pass
                except AttributeError:
                    print('This plugin can not be used by this bot')
                    return 'This plugin can not be used by this bot'

    return True
