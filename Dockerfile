FROM python:3
EXPOSE 5000
ADD src ./

RUN pip install -r requirements.txt

RUN pip install git+https://Codeiain1@bitbucket.org/broodmotherindustries/shared.git
RUN pip install git+https://Codeiain1@bitbucket.org/broodmotherindustries/botcore.git

CMD [ "python", "./main.py" ]